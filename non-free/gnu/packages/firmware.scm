(define-module (non-free gnu packages firmware)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public ath10k-firmware-non-free
  (package
   (name "ath10k-firmware-non-free")
   (version "20210919")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
		  (commit version)))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "1ix43qqpl5kvs6xpqrs3l5aj6vmwcaxcnv8l04mqqkyi9wamjydn"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
		  (use-modules (guix build utils))
		  (let ((source (assoc-ref %build-inputs "source"))
			(fw-dir (string-append %output "/lib/firmware/")))
		    (mkdir-p fw-dir)
		    (copy-recursively (string-append source "/ath10k")
				      (string-append fw-dir "/ath10k"))
		    #t))))
   (home-page "")
   (synopsis "")
   (description "")
   (license #f)))

ath10k-firmware-non-free
